# Grafički prikaz fajlova
### Konvertovanje XML, JSON i YAML u D3
Ovaj program pravi grafički prikaz od XML, JSON i YAML. Urađen je po Server-Klijent modelu. Za 
framework koristio se Django . Za backend se koristio Python a za frontend JavaScript , 
HTML, i CSS. <br />
### <b>Originalna verzija ovog projekta na Gitlab-u je obrisana, ovde se nalazi kopija originala.<b>
# Pokretanje i korišćenje
Python verzija potrebna za pokretanje i razvoj je 3.6. Poželjno je koristiti virtualno okruženje jer stvari inače lako mogu krenuti po zlu (koristili smo Powershell sa administatorskim privilegijama za izvršavanje ovih komandi). To bismo ovako nekako uradili:
```
python -m venv venv
```
i zatim to virtualno okruženje aktivirali:
```
venv\Scripts\activate.ps1
```
Ako vam ne daje da pokrenete script onda u powershell kao admin:
```
set-executionpolicy remotesigned
```
Sa aktiviranim virtualnim okruženjem konačno možemo da instaliramo potrebne pakete (ovde instaliramo Django!):
```
pip install -r requirements.txt
```
Aplikacija se onda može pokrenuti:
```
python manage.py runserver
```
Imate i superuser-a preko kojeg možete da vidite sve podatke:
```
python manage.py createsuperuser
```
Instaliranje plugin-a:<br/>
<b>Core se mora instalirati da bi se program pokrenuo!!!</b>
- Core
```
cd Core
```
```
python setup.py install
```

- Ucitavanje preko JSON-a
```
cd UcitavanjeJSON
```
```
python setup.py install
```

- Ucitavanje preko XML-a
```
cd UcitavanjeXML
```
```
python setup.py install
```
- Ucitavanje preko YAML
```
cd UcitavanjeYAML
```
```
python setup.py install
```

- Vizuelizacija D3 Simple:
```
cd VizuelizacijaD3Simple
```
```
python setup.py install
```
-Vizuelizacija D3 Complex:

```
cd VizuelizacijaD3Complex
```
```
python setup.py install
```

Brisanje plugin-a:
```
py -m pip uninstall core
py -m pip uninstall json-ucitavanje-fajla
py -m pip uninstall xml-ucitavanje-fajla
py -m pip uninstall yaml-ucitavanje-fajla
py -m pip uninstall vizuelizacija-D3-jednostavan
py -m pip uninstall vizuelizacija-D3-slozena
```
# Autori
- Jovan Ćorilić (SW 48/2017)
- Bogdan Čiplić (SW 79/2017)
- Bubnjević Mihailo (SW 71/2016)
- Miletić Bogdana (SW 56/2017)
- Vučenović Srbislav (SW 77/2017)
