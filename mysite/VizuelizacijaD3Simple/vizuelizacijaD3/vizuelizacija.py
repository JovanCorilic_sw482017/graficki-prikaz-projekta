from Core.core.models import Graph
from Core.core.services.vizualizuj import VizualizujService

class VizuelizacijaD3Simple(VizualizujService):
    def naziv(self):
        return "Vizualizuj podatke D3 simple"
    def identifier(self):
        return "vizualizuj_D3_simple"

    def vizualizuj(self, graf : Graph()):
        temp = Graph()
        temp = graf

        for dex in temp.nodes:
            dex.id -= 1
        for dex in temp.edges:
            dex.origin -= 1
            dex.destination -= 1
        #ucitavanje fajla kao string
        f = open("VizuelizacijaD3Simple/vizuelizacijaD3/templates/vizuelizator_jednostavan.html","r")

        odgovor = f.readlines()

        str1 = ' '.join(str(e) for e in odgovor)

        #pravljenje stringa za cvorove

        listacvorova = ""

        for i in temp.nodes:
            listacvorova += "{ " + str(i.id) + ":" + '"' + i.element + "|"+i.attribute+ '"' + "," + '"group"' + ":" + str(1) + "},"
        listacvorova = listacvorova[:len(listacvorova)-1]

        #pravljenje stringa za linkove

        listaVeza = ""
        for i in temp.edges:
            listaVeza+="{source: "+str(i.origin)+",target: "+str(i.destination)+"},"
        listaVeza = listaVeza[:len(listaVeza)-1]

        #delovi gde se upisuju fajlovi
        str1=str1.replace("PLACEHOLDERNODES",listacvorova)
        str1=str1.replace("PLACEHOLDERLINKS",listaVeza)

        #kako izgleda
        #nodes = {1 :"Jovan"},{2: "Aca"}, {3: "Luka"},{4: "Akson"},{5: "Ajaks"},{6: "Nikola"}
        #links = {source: 1,target: 2},{source: 1,target: 3},{source: 1,target: 4},{source: 5,target: 0}
        return str1



