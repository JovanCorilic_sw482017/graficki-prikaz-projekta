from setuptools import setup, find_packages
setup(
    name="vizuelizacija-D3-slozena",
    version="0.1",
    packages=find_packages(),
    namespace_packages=['vizuelizacijaD3'],
    install_requires=['core>=0.1'],
    entry_points = {
        'plugin.vizualizuj':
            ['vizuelizujD3Complex=VizuelizacijaD3Complex.vizuelizacijaD3.vizuelizacija:VizuelizacijaD3Complex'],
    },
    zip_safe=True
)