from Core.core.models import Graph
from Core.core.services.vizualizuj import VizualizujService

#importujemo model grafa iz django core aplikacije.

class VizuelizacijaD3Complex(VizualizujService):
    def naziv(self):
        return "Vizualizuj podatke D3 complex"


    def identifier(self):
        return "vizualizuj_d3_complex"


    def vizualizuj(self, graf : Graph()):
        temp = Graph()
        temp = graf

        ######################kontrolni ispisi##################



        for dex in temp.nodes:
            dex.id -= 1
        for dex in temp.edges:
            dex.origin -= 1
            dex.destination -= 1



        ########################################################################


        #ucitavanje fajla kao string
        f = open("VizuelizacijaD3Complex/vizuelizacijaD3/templates/vizuelizator_slozen.html","r")

        odgovor = f.readlines()

        #string ucitanih linija iz fajla

        str1 = ' '.join(str(e) for e in odgovor)

        #string koji predstavlja cvorove(niz json objekata)

        listacvorova = ""

        for i in temp.nodes:
            listacvorova += "{ " + str(i.id) + ":" + '"' + i.element + '"' + "," + '"atributi"' + ":" + '"' + i.attribute + '"' + "," + "id" + ":" + '"' + "id" + str(i.id) + '"' + "},"


        listacvorova = listacvorova[:len(listacvorova)-1]

        #string koji predstavlja grane(niz json objekata)

        listaVeza = ""
        for i in temp.edges:
            listaVeza+="{source: "+str(i.origin)+",target: "+str(i.destination)+"},"
        listaVeza = listaVeza[:len(listaVeza)-1]  #odsecanje zareza sa kraja

        #upis stringova u fajl(stringovi koji predstavljaju cvorove i grane grafa)
        str1=str1.replace("ZAMENA_CVOROVA",listacvorova)
        str1=str1.replace("ZAMENA_GRANA",listaVeza)


        return str1
