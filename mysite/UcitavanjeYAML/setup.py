from setuptools import setup, find_packages
setup(
    name="yaml-ucitavanje-fajla",
    version="0.1",
    packages=find_packages(),
    namespace_packages=['ucitavanjeYAML'],
    install_requires=['core>=0.1'],
    entry_points={
        'plugin.ucitati':
            ['ucitaj_yaml=UcitavanjeYAML.ucitavanjeYAML.ucitavanje_yaml:UcitatiYAMLFajl'],
    },
    zip_safe=True
)
