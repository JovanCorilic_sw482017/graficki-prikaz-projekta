from Core.core import Graph
from Core.core.services import UcitatiService
import yaml


class UcitatiYAMLFajl(UcitatiService):
    def __init__(self):
        self.counter = 1

    def naziv(self):
        return "Ucitati podatke iz yaml fajla"

    def identifier(self):
        return "ucitati_yaml_fajl"

    def ucitati(self, file):
        graph = Graph()

        with open(file, 'r') as openedFile:
            data = openedFile.read()
        openedFile.close()
        loadedyaml = yaml.safe_load(data)
        dictData = {file.__str__(): loadedyaml}
        self.counter = 1
        father = 1
        # skinemo fajl na vrhu i ubacimo ga kao koren
        for key, value in dictData.items():
            graph.insert_node(self.counter, key, "")

            self.counter += 1
            graph = self.loadGraphRecursive(value, father, graph)
        return graph

    def loadGraphRecursive(self, data, father, graph):
        # ako je dict
        if isinstance(data, dict):
            for key, value in data.items():

                if isinstance(value, dict) or isinstance(value, list):

                    graph.insert_node(self.counter, key, key)
                    graph.insert_edge(father, self.counter)

                    self.counter += 1
                    father = father + 1
                    self.loadGraphRecursive(value, father, graph)

                else:
                    father = self.counter - 1

                    graph.insert_node(self.counter, value, value)
                    graph.insert_edge(father, self.counter)

                    self.counter += 1
        # ako je lista
        elif isinstance(data, list):
            father = self.counter - 1
            for item in data:
                if isinstance(item, dict):

                    self.loadGraphRecursive(item, father, graph)

                else:
                    graph.insert_node(self.counter, item, item)
                    graph.insert_edge(father, self.counter)

                    self.counter += 1


        else:
            return graph
        return graph


if __name__ == '__main__':
    yamlLoader = UcitatiYAMLFajl()
    graph = yamlLoader.ucitati("test.yaml")