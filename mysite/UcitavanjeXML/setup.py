from setuptools import setup, find_packages
setup(
    name="xml-ucitavanje-fajla",
    version="0.1",
    packages=find_packages(),
    namespace_packages=['ucitavanjeXML'],
    install_requires=['core>=0.1'],
    entry_points={
        'plugin.ucitati':
            ['ucitaj_xml=UcitavanjeXML.ucitavanjeXML.ucitavanje_xml:UcitatiXMLFajl'],
    },
    zip_safe=True
)
