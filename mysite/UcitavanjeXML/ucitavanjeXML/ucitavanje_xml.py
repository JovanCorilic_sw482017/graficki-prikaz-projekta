import xml.etree.ElementTree as ET
from Core.core.models import Graph
from Core.core.services.ucitati import UcitatiService


def iterator(veze, pocetak, predak):
    if predak is None:
        for a in list(pocetak):
            # print([a.tag, ["niko", -1]])
            veze += [[1, a.tag[1]]]
            iterator(veze, list(a), a)
    else:
        for a in list(predak):
            # print([a.tag, predak.tag])
            veze += [[predak.tag[1], a.tag[1]]]
            iterator(veze, list(a), a)


def iteratorrekurzije(veze, pocetak, predak, referenca, koga_vezuje, id):
    id -= 1
    objekat = referenca[id]
    if id + 1 == len(referenca):
        veze += [[koga_vezuje, predak.tag[1]]]
        print("AAAAAAAAAA")
        return

    id += 1
    objekat = referenca[id]

    for a in list(predak):
        if objekat == a.tag[0]:
            if id + 1 == len(referenca):
                veze += [[koga_vezuje, a.tag[1]]]
                print("AAAAAAAAAA")
                return
            else:
                print("Iteracija " + str(id) + ") " + objekat + "  " + str(len(list(a))))
                iteratorrekurzije(veze, list(a), a, referenca, koga_vezuje, id + 1)

        try:
            objekat2 = referenca[id + 1]
            print("Iteracija2 " + str(id) + ") " + objekat2 + "   " + str(a.text))
            if objekat2 == a.text:
                if objekat == a.tag[0]:
                    veze += [[koga_vezuje, a.tag[1]]]
                    print("AAAAAAAAAA")
                    return
        except:
            pass


def rekurzija(veze, lista, cvorovi, stablo):
    for element in lista:
        if 'references' in element.attrib:
            koga_vezuje = element.tag[1]
            # print(koga_vezuje)

            # print(element.attrib['references'])
            ref = element.attrib['references'].split(",")

            for r in ref:
                referenca = r.strip()
                referenca = referenca.split("/")

                iteratorrekurzije(veze, stablo.getroot(), stablo.getroot(), referenca, koga_vezuje, 1)



class UcitatiXMLFajl(UcitatiService):
    def naziv(self):
        return "Ucitati podatke iz xml fajla"
    def identifier(self):
        return "ucitati_xml_fajl"

    def ucitati(self, lokacija):
        brojac = 0
        cvorovi = []
        veze = []
        graf = Graph()
        try:
            stablo = ET.parse(lokacija)
            pocetak = stablo.getroot()
            lista = list(stablo.iter())

            print(lista.__len__())

            for element in lista:
                brojac += 1
                if element.text is None or element.text == '\n':
                    element.text = ""
                cvorovi.append([brojac, element.tag, element.text, 1])
                element.tag = [element.tag, brojac]

                # print("ele, " + str( element.attrib ))

            iterator(veze, pocetak, None)

            rekurzija(veze, lista, cvorovi, stablo)

            # return [stablo, cvorovi, veze]
            for node in cvorovi:
                graf.insert_node(node[0], node[1], node[2])
                print(node[2])
            for edge in veze:
                graf.insert_edge(edge[0], edge[1])
            return graf

        except:
            return graf
