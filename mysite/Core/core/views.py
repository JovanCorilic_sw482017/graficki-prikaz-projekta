from django.apps.registry import apps
from django.shortcuts import render
from Core.core.sidetree import formiraj
import copy
# Create your views here.
from Core.core.forms import FajlForm
from django.http import HttpResponse
import json


def index(request):
    plugini = apps.get_app_config('core').plugini_ucitavanje
    pluginiVizuelizuj = apps.get_app_config('core').plugini_vizuelizacija
    #request.session['pluginiUcitavanje'] = plugini
    #request.session['pluginiVizuelizacija'] = pluginiVizuelizuj
    context = {}
    context['form'] = FajlForm()
    return render(request,"index.html",{"title":"Index","plugini_ucitavanje":plugini,
                                        "plugini_vizuelizacija":pluginiVizuelizuj})

def ucitavanje_plugin(request):
    try:
        request.session['izabran_plugin_ucitavanje']=request.POST['ucitaj']
        id = request.POST['ucitaj']
        apps.get_app_config('core').trenutni_plugin_ucitavanje=id;
        #lokacija2 = File
        poruka = ""

        handle_uploaded_file(request.FILES["lokacijaFajl"])
        plugini=apps.get_app_config('core').plugini_ucitavanje

        for i in plugini:
            if i.identifier() == id:
                apps.get_app_config('core').trenutni_fajl = "Files/"+request.FILES['lokacijaFajl'].name
                apps.get_app_config('core').graf=i.ucitati("Files/"+request.FILES['lokacijaFajl'].name)#<-Ovde putanja
    except:
        print("Fali nesto")

    plugini = apps.get_app_config('core').plugini_ucitavanje
    pluginiVizuelizuj = apps.get_app_config('core').plugini_vizuelizacija
    try:
        graf = copy.deepcopy(apps.get_app_config('core').graf)
        for dex in graf.nodes:
            dex.id -= 1
        for dex in graf.edges:
            dex.origin -= 1
            dex.destination -= 1

        rrtree = formiraj(graf.nodes, graf.edges)
    except:
        rrtree = None

    return render(request, "index.html", {"title": "Index", "plugini_ucitavanje": plugini,
                                          "plugini_vizuelizacija": pluginiVizuelizuj,
                                           "rrtree": rrtree})

def handle_uploaded_file(f):
    with open("Files/"+f.name, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

def restujIdGrafa(graf):
    for dex in graf.nodes:
        dex.id += 1
    for dex in graf.edges:
        dex.origin += 1
        dex.destination += 1

#Za sada posto nema primera, napravljen je primer grafa dole da uzme, posle ce biti ispravljeno kada se naprave primeri pravi
def vizuelizacija_plugin(request):
    try:
        request.session['izabran_plugin_vizuelizacija'] = request.POST['iscrtaj']
        id = request.POST['iscrtaj']
        apps.get_app_config('core').trenutni_plugin_vizuelizacija = id
        plugini = apps.get_app_config('core').plugini_vizuelizacija
        for i in plugini:
            if i.identifier()==id:

                for node in apps.get_app_config('core').graf.nodes:
                    if(node.id == 0):
                        restujIdGrafa(apps.get_app_config('core').graf)
                        break

                graf = copy.deepcopy(apps.get_app_config('core').graf)
                temp = i.vizualizuj(graf)
                try:
                    rrtree = formiraj(graf.nodes, graf.edges)
                except:
                    rrtree = None

                #print(temp)
    except:
        temp=""
        rrtree = None
    plugini = apps.get_app_config('core').plugini_ucitavanje
    pluginiVizuelizuj = apps.get_app_config('core').plugini_vizuelizacija

    return render(request, "index.html", {"title": "Index", "plugini_ucitavanje": plugini,
                                          "plugini_vizuelizacija": pluginiVizuelizuj,
                                          "stablo": temp, "rrtree": rrtree})


def search(request):
    atribut = request.POST.get("search")
    plugini = apps.get_app_config('core').plugini_ucitavanje
    for i in plugini:
        if i.identifier() == apps.get_app_config('core').trenutni_plugin_ucitavanje:
            apps.get_app_config('core').graf = i.ucitati(apps.get_app_config('core').trenutni_fajl)
    noviGRAF=copy.deepcopy(apps.get_app_config('core').graf)
    pluginiVizuelizuj = apps.get_app_config('core').plugini_vizuelizacija
    try:
        noviGRAF.search(atribut)

        temp=""
        for i in pluginiVizuelizuj:
            if i.identifier() == apps.get_app_config('core').trenutni_plugin_vizuelizacija:
                temp = i.vizualizuj(noviGRAF)
    except:
        temp=""

    try:
        rrtree = formiraj(noviGRAF.nodes, noviGRAF.edges)
    except:
        rrtree=None
    return render(request, "index.html", {"title": "Index", "plugini_ucitavanje": plugini,
                                          "plugini_vizuelizacija": pluginiVizuelizuj,
                                          "stablo": temp, "rrtree": rrtree})

def filter(request):
    nazivAtributa = request.POST.get("nazivAtributa")
    operator = request.POST.get("operator")
    vrednostAtributa = request.POST.get("vrednostAtributa")
    plugini = apps.get_app_config('core').plugini_ucitavanje
    for i in plugini:
        if i.identifier() == apps.get_app_config('core').trenutni_plugin_ucitavanje:
            apps.get_app_config('core').graf = i.ucitati(apps.get_app_config('core').trenutni_fajl)
    filtriraniGraf = copy.deepcopy(apps.get_app_config('core').graf)
    pluginiVizuelizuj = apps.get_app_config('core').plugini_vizuelizacija
    try:
        filtriraniGraf.filter(nazivAtributa, operator, vrednostAtributa)

        temp=""
        for i in pluginiVizuelizuj:
            if i.identifier() == apps.get_app_config('core').trenutni_plugin_vizuelizacija:
                temp = i.vizualizuj(filtriraniGraf)
    except:
        temp=""

    try:
        rrtree = formiraj(filtriraniGraf.nodes, filtriraniGraf.edges)
    except:
        rrtree=None
    return render(request, "index.html", {"title": "Index", "plugini_ucitavanje": plugini,
                                          "plugini_vizuelizacija": pluginiVizuelizuj,
                                          "stablo": temp, "rrtree": rrtree})
def identify(request):
    id=request.POST.get("identify")
    element="Nema elementa"
    atribut="Nema atributa"
    try:
        graf=apps.get_app_config('core').graf
        nid=int(id)+1
        node=graf.findNode(nid)
    except:
        node=None
    if node!=None:
        element=node.element
        atribut=node.attribute
    return HttpResponse(json.dumps({'id': id,'element': element,'atribut': atribut}))
