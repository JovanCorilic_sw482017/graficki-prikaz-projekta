from abc import abstractmethod, ABC


class VizualizujService(ABC):
    @abstractmethod
    def naziv(self):
        pass

    @abstractmethod
    def identifier(self):
        pass

    @abstractmethod
    def vizualizuj(self, graf):
        pass