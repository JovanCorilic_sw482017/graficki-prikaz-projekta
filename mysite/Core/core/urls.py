from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('ucitavanje/plugin', views.ucitavanje_plugin, name="ucitavanje_plugin"),
    path('vizuelizacija/plugin', views.vizuelizacija_plugin, name="vizuelizacija_plugin"),
    path('search', views.search, name='search'),
    path('identify', views.identify, name='identify'),
    path('filter', views.filter, name='filter')

    ]