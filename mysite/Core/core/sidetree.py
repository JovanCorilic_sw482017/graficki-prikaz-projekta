
def formiraj(cvorovi, veze):
    br = 0
    povezani = [cvorovi[br]]

    tekst = str(cvorovi[br].id) + ") " + cvorovi[br].element
    opis = ""
    if not(cvorovi[br].attribute.strip() == "" or cvorovi[br].attribute.strip() == '\n'):
        opis = " (" + cvorovi[br].attribute + ")"

    stri = "<ul><li> <input id=\"" + str(cvorovi[br].id) + "\" onclick=\"" + "oznaciStablo(" + str(cvorovi[br].id) + ")" + "\" name=\"" \
    + "cvorstabla" + "\" type=\"button\" style=\"border: none;\" value=\"" + tekst + "\"></input>" + opis + "</li>"

    for dete in getchildren(cvorovi[br].id, cvorovi, veze):
        if dete not in povezani:
            povezani.append(dete)
            stri += dodaj(cvorovi, veze, dete, povezani)
        else:
            stri += sad_european(dete)
    stri += "</ul>"

    while povezani.__len__() != len(cvorovi):
        ms = maloStablo(cvorovi, povezani, veze)
        stri += ms[0]
        povezani = ms[1]

    return stri + "<style>"\
".slurp {"\
  "background-color: cyan;"\
"}"\
"</style>"\
"<script>"\
"function oznaciStablo(id) {"\
   "selektovanKrug(id);"\
   "var x = document.getElementsByName(\"cvorstabla\");"\
   "var i = 0;"\
   "for (i = 0; i < x.length; i++) {"\
     "if (x[i].id == id && !x[i].classList.contains(\"slurp\")) {"\
       "x[i].classList.add(\"slurp\");"\
     "}"\
     "else{"\
       "x[i].classList.remove(\"slurp\");"\
     "}"\
   "}"\
"}"\
"</script>"


def getchildren(id, cvorovi, veze):
    niz = []
    odg = []

    for i in veze:
        if i.endpoints()[0] == id:
            niz.append(i.endpoints()[1])

    for i in cvorovi:
        if i.id in niz:
            odg.append(i)

    return odg


def dodaj(cvorovi, veze, cvor, povezani):
    br = cvor.id

    tekst = str(cvorovi[br].id) + ") " + cvorovi[br].element
    opis = ""
    if not(cvorovi[br].attribute.strip() == "" or cvorovi[br].attribute.strip() == '\n'):
        opis = " (" + cvorovi[br].attribute + ")"

    stri ="<ul><li> <input id=\"" + str(cvor.id) + "\" onclick=\"" + "oznaciStablo(" + str(cvor.id) + ")" + "\" name=\"" \
        + "cvorstabla" + "\" type=\"button\" style=\"border: none;\" value=\"" + tekst + "\"></input>" + opis + "</li>"

    for i in getchildren(cvor.id, cvorovi, veze):
        if i not in povezani:
            povezani.append(i)
            stri += dodaj(cvorovi, veze, i, povezani)
        else:
            stri += sad_european(i)
    return stri + "</ul>"


def sad_european(cvor):
    br = cvor.id

    tekst = str(cvor.id) + ") " + cvor.element
    opis = ""
    if not(cvor.attribute.strip() == "" or cvor.attribute.strip() == '\n'):
        opis = " (" + cvor.attribute + ")"

    stri = "<ul><li style=\"color:gray; font-style:italic\"> <input id=\"" + str(cvor.id) + "\" onclick=\"" + "oznaciStablo(" + str(cvor.id) + ")" + "\" name=\"" \
    + "cvorstabla" + "\" style=\"color:gray; font-style:italic\" type=\"button\" style=\"border: none;\" value=\"" + tekst + "\"></input>" + opis + "</li>"

    return stri + "</ul>"


def maloStablo(cvorovi2, povezani, veze):
    stri = "<br />"
    cvorovi = []
    for cvor in cvorovi2:
        print(cvor.id)
        if cvor not in povezani:
            cvorovi.append(cvor)


    br = 0
    povezani.append(cvorovi[br])
    tekst = str(cvorovi[br].id) + ") " + cvorovi[br].element
    opis = ""
    if not (cvorovi[br].attribute.strip() == "" or cvorovi[br].attribute.strip() == '\n'):
        opis = " (" + cvorovi[br].attribute + ")"

    stri += "<ul><li> <input id=\"" + str(cvorovi[br].id) + "\" onclick=\"" + "oznaciStablo(" + str(
        cvorovi[br].id) + ")" + "\" name=\"" \
           + "cvorstabla" + "\" type=\"button\" style=\"border: none;\" value=\"" + tekst + "\"></input>" + opis + "</li>"

    for dete in getchildren(cvorovi[br].id, cvorovi, veze):
        print(str(dete.id) + "je u p?" + str(dete in povezani))
        if dete not in povezani:
            povezani.append(dete)
            stri += dodaj2(cvorovi, veze, dete, povezani)
        else:
            stri += sad_european(dete)
    stri += "</ul>"

    return [stri, povezani]


def dodaj2(cvorovi, veze, cvor, povezani):
    br = 0

    ite = 0
    for cvorr in cvorovi:
        if cvorr.id == cvor.id:
            br = ite
        else:
            ite += 1


    tekst = str(cvorovi[br].id) + ") " + cvorovi[br].element
    opis = ""
    if not(cvorovi[br].attribute.strip() == "" or cvorovi[br].attribute.strip() == '\n'):
        opis = " (" + cvorovi[br].attribute + ")"

    stri ="<ul><li> <input id=\"" + str(cvor.id) + "\" onclick=\"" + "oznaciStablo(" + str(cvor.id) + ")" + "\" name=\"" \
        + "cvorstabla" + "\" type=\"button\" style=\"border: none;\" value=\"" + tekst + "\"></input>" + opis + "</li>"

    for i in getchildren(cvor.id, cvorovi, veze):
        if i not in povezani:
            povezani.append(i)
            stri += dodaj(cvorovi, veze, i, povezani)
        else:
            stri += sad_european(i)
    return stri + "</ul>"
