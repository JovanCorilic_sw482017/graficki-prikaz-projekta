#from django.db import models
# Create your models here.


class Graph:

    class Node:

        def __init__(self,id, x, attribute=None):
            self.id = id
            self.element = x
            self.attribute = attribute

        def id(self):
            return self.id

        def element(self):
            return self.element

        def attribute(self):
            return self.attribute

        def __str__(self):
            return str(str(self.id)+" "+self.element+" "+self.attribute)

    class Edge:

        def __init__(self, origin, destination, element):
            self.origin = origin
            self.destination = destination

        def endpoints(self):
            return self.origin, self.destination

        def __str__(self):
            return '({0},{1})'.format(self.origin, self.destination)

    def __init__(self):
        self.nodes = []
        self.edges = []

    def insert_node(self,id, x=None,y=None):
        v = self.Node(id,x, y)
        self.nodes.append(v)

    def insert_edge(self, u, v, x=None):
        e = self.Edge(u, v, x)
        self.edges.append(e)

    def search(self, name):
        nodovi=[]
        for i in self.nodes:
            nodovi.append(i)
        edgevi=[]
        for i in self.edges:
            edgevi.append(i)
        self.nodes.clear()
        self.edges.clear()
        for n in nodovi:
            if name in str(n.attribute):
                self.nodes.append(n)
        for e in edgevi:
            for n in self.nodes:
                if e.origin==n.id:
                    for nn in self.nodes:
                       if e.destination == nn.id:
                            self.edges.append(e)
        br=1
        zamjena={}
        for n in self.nodes:
            zamjena[n.id]=br
            n.id=br
            br+=1
        for n in self.edges:
            n.origin=zamjena[n.origin]
            n.destination=zamjena[n.destination]
    def findNode(self,id):
        nod = self.Node(id,"Nema elemnta","Nema atributa")
        for node in self.nodes:
            if id == node.id:
                nod.element=node.element
                nod.attribute=node.attribute
        return nod
    def filter(self, naziv, operator, vrednost):
        nodes = []
        for node in self.nodes:
            nodes.append(node)
        edges = []
        for edge in self.edges:
            edges.append(edge)
        self.nodes.clear()
        self.edges.clear()
        for node in nodes:
            if node.element == naziv:
                if operator == ">":
                    if node.attribute > vrednost:
                        self.nodes.append(node)
                elif operator == "<":
                    if node.attribute < vrednost:
                        self.nodes.append(node)
                elif operator == "<=":
                    if node.attribute <= vrednost:
                        self.nodes.append(node)
                elif operator == ">=":
                    if node.attribute >= vrednost:
                        self.nodes.append(node)
                elif operator == "==":
                    if node.attribute == vrednost:
                        self.nodes.append(node)
                elif operator == "!=":
                    if node.attribute != vrednost:
                        self.nodes.append(node)
        for e in edges:
            for n in self.nodes:
                if e.origin == n.id:
                    for nn in self.nodes:
                        if e.destination == nn.id:
                            self.edges.append(e)

        counter = 1
        zamena = {}
        for n in self.nodes:
            zamena[n.id] = counter
            n.id = counter
            counter += 1
        for n in self.edges:
            n.origin = zamena[n.origin]
            n.destination = zamena[n.destination]