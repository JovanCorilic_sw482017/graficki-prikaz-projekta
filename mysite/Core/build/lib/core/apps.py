import pkg_resources
from django.apps import AppConfig

class CoreConfig(AppConfig):
    name = 'core'
    plugini_ucitavanje=[]
    plugini_vizuelizacija=[]
    trenutni_plugin_ucitavanje=""
    trenutni_plugin_vizuelizacija = ""
    graf=None
    trenutni_fajl=""

    def ready(self):
        self.plugini_ucitavanje=load_plugins("plugin.ucitati")
        self.plugini_vizuelizacija=load_plugins("plugin.vizualizuj")

def load_plugins(oznaka):
    plugins = []
    for ep in pkg_resources.iter_entry_points(group=oznaka):
        p = ep.load()
        print("{} {}".format(ep.name, p))
        plugin = p()
        plugins.append(plugin)
    return plugins