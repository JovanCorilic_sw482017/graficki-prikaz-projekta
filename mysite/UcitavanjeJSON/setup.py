from setuptools import setup, find_packages
setup(
    name="json-ucitavanje-fajla",
    version="0.1",
    packages=find_packages(),
    namespace_packages=['ucitavanjeJSON'],
    install_requires=['core>=0.1'],
    entry_points = {
        'plugin.ucitati':
            ['ucitaj_json=UcitavanjeJSON.ucitavanjeJSON.ucitavanje_json:UcitatiJSONFajl'],
    },
    zip_safe=True
)