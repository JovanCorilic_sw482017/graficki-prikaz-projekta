from Core.core import Graph
from Core.core.services import UcitatiService
import json



class UcitatiJSONFajl(UcitatiService):
    def naziv(self):
        return "Ucitati podatke iz json fajla"
    def identifier(self):
        return "ucitati_json_fajl"

    def ucitati(self,fajl):
        graf=Graph()
        try:
            with open(fajl, 'r') as myfile:
                data = myfile.read()
            myfile.close()
            recnik = json.loads(data)
            #recnik={fajl.__str__():recnik}
            print(recnik);
            br = 0
            reference=[]
            if "references" in recnik:
                reference=recnik["references"]
                del recnik["references"]
            rekurzija(br, recnik, graf, reference,father=-1)
            nodovi=[]
            for i in graf.nodes:
                nodovi.append(i)
            graf.nodes.clear()
            for i in nodovi:
                if len(i) == 3:
                    graf.insert_node(i[0], i[1], str(i[2]))
                else:
                    graf.insert_node(i[0], i[1], "")
                br += 1
            edgevi=[]
            for i in graf.edges:
                edgevi.append(i)
            graf.edges.clear()
            for i in edgevi:
                graf.insert_edge(i[0],i[1])
            #print(reference)
            graf=dodjelaReferenci1(graf,reference)
            for deks in graf.nodes:
                deks.id += 1
            for deks in graf.edges:
                deks.origin += 1
                deks.destination += 1

            return graf
        except:
            return graf
def dodjelaReferenci1(graf, reference):
    korenovi=[]
    veze=[]
    for i in graf.nodes:
        korenovi.append(i.id)
    for i in graf.edges:
        if i.destination in korenovi:
            korenovi.remove(i.destination)
    for i in reference:
        od=-1
        do=-1
        for a in (i["from"]).split("/"):
            mogucnosti = []
            for nod in graf.nodes:
                if nod.element == a:
                    mogucnosti.append(nod.id)
            if od == -1:
                for k in korenovi:
                    if k in mogucnosti:
                        od = k
                        break
            else:
                for edge in graf.edges:
                    if edge.destination in mogucnosti and od == edge.origin:
                        od = edge.destination
                        break
            #print(od)
        for a in (i["to"]).split("/"):
            mogucnosti = []
            for nod in graf.nodes:
                if nod.element == a:
                    mogucnosti.append(nod.id)
            if do == -1:
                for k in korenovi:
                    if k in mogucnosti:
                        do = k
                        break
            else:
                for edge in graf.edges:
                    if edge.destination in mogucnosti and do == edge.origin:
                        do = edge.destination
                        break
        veze.append([od,do])
    for i in veze:
        graf.insert_edge(i[0],i[1])
    return graf
def dodjelaReferenci2(graf, reference):
    korenovi=[]
    for i in graf.nodes:
        korenovi.append(i.id)
    for i in graf.edges:
        if i.destination in korenovi:
            korenovi.remove(i.destination)
    for i in reference:
        for ii in i[1]:
            origin=i[0]
            zadnji=-1
            for iii in ii["reference"].split("/"):
                if len(iii.split("["))==1:
                    mogucnosti=[]
                    for nod in graf.nodes:
                        if nod.element==iii:
                            mogucnosti.append(nod.id)
                    if zadnji==-1:
                        for k in korenovi:
                            if k in mogucnosti:
                                zadnji=k
                                #break
                    else:
                        for edge in graf.edges:
                            if edge.destination in mogucnosti and zadnji==edge.origin:
                                zadnji=edge.destination
                                #break

                else:
                    print("treba za listu")
            #graf.[origin,zadnji])
            graf.insert_edge(origin, zadnji)
    return graf
def rekurzija(br,stablo,graf,reference,father):
    x = isinstance(stablo, list)  #stablo json podataka
    if x==False:
        for i in stablo:
            if(i=="references"):
                reference.append([father,stablo[i]])
            else:
                y = isinstance(stablo[i],dict)
                x = isinstance(stablo[i],list)
                if x==False and y==False:
                    graf.nodes.append([br, i,stablo[i]])
                    if father!=-1:
                        graf.edges.append([father,br])
                    br+= 1
                else:
                    c=False
                    for a in stablo[i]:
                        if False == isinstance(a, dict) and False == isinstance(a, list):
                            #print (stablo[i])
                            c=True
                            break
                    if x == True and c==True:
                        graf.nodes.append([br, i, stablo[i]])
                        if father != -1:
                            graf.edges.append([father, br])
                        br += 1
                    else:
                        graf.nodes.append([br,i])
                        if father!=-1:
                            graf.edges.append([father,br])
                        br+=1
                        br,stabla,graf,reference,fatheri=rekurzija(br,stablo[i],graf,reference,father=br-1)
    else:
        brojac = 0
        ime=""
        for i in graf.nodes:
            if father==i[0]:
                ime=i[1]
        for i in stablo:
            if True == isinstance(i,dict) or True ==isinstance(i,list):
                i={ime+"["+str(brojac)+"]":i}
                brojac+=1
                br, stabla, graf,reference,fatheri = rekurzija(br, i, graf,reference,father)
    return (br,stablo,graf,reference,father)



if __name__ == '__main__':
    u=UcitatiJSONFajl()
    graf=u.ucitati("C:/Users/TehnoCentar/Desktop/primer.json")
    for i in graf.nodes:
        print(i)
    for i in graf.edges:
        print(i)